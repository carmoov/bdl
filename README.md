# Basic Development Library

Basic development library. Suite of low level Go libraries, third party forks and methodology.
To see the documentation:
```
$ godoc -http=:6060 -goroot .
```
then access it at http://localhost:6060/pkg/

## Library content

### src

#### src/crypto

Bcrypt tools.

#### src/net

Networking packages.

#### src/net/http

Tools for Go HTTPRequest parsing.

#### src/net/rpc

Implementation of RPC 2.0 standard requests, responses and errors.

#### src/sync

Synchronization tools such as mutex pool.

#### src/tools

Set of tools for working with:
- array: Tools for working with arrays.
- mapmarshaller: Tools for marshaling and unmarsaling maps. Wrapper for mitchellh/mapstructure.
- noncegenerator: Tool for generating alphanumeric strings.
- number: Number format transformation.
- ptr: pointer generation for strings and numbers.
- string: Tools for working with strings.
- time: Time tools, generate now datetime in timestamp etc.
- uuid: wrapper for google uuid library, includes uuid generation with timestamp.
- validation: validation of emails, phone numbers etc.

### test

Tests for bdl components.

## Third party forks, allowed dependencies

Wrap external dependencies libraries so they can be substituted or re-written in case of not maintenance.
Allways fork repositories to prevent from incompatibilities and project deprecations.
Pull from origin only after testing and ensuring there is no incompatibilities.
Prioritize stability and security over novelty.

### Google uuid

Go package for UUIDs based on RFC 4122 and DCE 1.1: Authentication and Security Services. 

wrapper:
gitlab.com/carmoov/bld/src/tools/uuid

master:
github.com/google/uuid

fork:
gitlab.com/carmoov/google-uuid

### Map decoder: mapstructure

Go library for decoding generic map values into native Go structures and vice versa. 

wrapper:
gitlab.com/carmoov/bld/src/tools/map

master:
github.com/mitchellh/mapstructure

fork:
gitlab.com/carmoov/mitchellh-mapstructure

Example:

```
var transaction transaction_model.Transaction
if uErr := map_tools.Unmarshal(rpcRequest.Params, &transaction); uErr != nil {
    logger.Warning("Error unmarshalling RPCRequest.Params, err => ", uErr)
    return 400, rpc_model.NewRPCErrorResponse(rpc_error.ERROR_PARSE_ERROR_CODE, rpc_error.ERROR_PARSE_ERROR_MESSAGE, rpc_error.ERROR_PARSE_ERROR_MEANING)
}
```

## Basic agreements

Low level testing, documentation and design agreements.

### Test

Unit test all components to meet standards.

### Document

In-file document all components to meet standards.

### Design

#### Use pointers in entities

We are in a web environment, so it is common the transformation of JSON object to "structs" objects and vice versa.

Using pointers in struct will prevent converters to auto-fill the values, for example prevent a null field of type string to be autofilled as empty string.

USING POINTERS (GOOD PRACTICE):

```
type User struct {
	Email      *string   `json:"email"`
	FirstName  *string   `json:"firstname"`
	LastName   *string   `json:"lastname"`
    Scope      *[]string `json:"scope"`
}


// json = {"email":"john.doe@example.com"}
var user User
err := json.Unmarshal(json, &user)
// result: User[Email:"john.doe@example.com",FirstName:nil,LastName:nil,Scope:nil]
// not existant values won't be transformed to empty strings and empty arrays.

user.Email = ptr_tools.String("john.doe@example.com")
json, err := json.Marshal(&user)
// result: {"email":"john.doe@example.com"}
// no nil values will appear
```

NOT USING POINTERS (BAD PRACTICE):

```
type User struct {
	Email       string   `json:"email"`
	FirstName   string   `json:"firstname"`
	LastName    string   `json:"lastname"`
    Scope       []string `json:"scope"`

}

// json = {"email":"john.doe@example.com"}
var user User
err := json.Unmarshal(json, &user)
// result: User[Email:"john.doe@example.com",FirstName:"",LastName:"",Scope:[]]
// not existant values will be transformed to empty strings and empty arrays.

user.Email = "john.doe@example.com"
json, err := json.Marshal(&user)
// result: {"email":"john.doe@example.com","firstName":"","lastName":"","scope":[]}
// nil values will appear as empty strings and empty arrays.
```

#### Dependency between layers

Prevent dependency between components of same layer and cyclic dependencies.

Example (GOOD PRACTICE):

```
                              ┌──────┐
                  ┌──────────⚫│ main │⚫───────────┐
                  │           └──────┘            │
                  │                               │
                  │                               │
        ┌─────────┴────────┐             ┌────────┴─────────┐                │
        │  EntityAHandler  │             │  EntityBHandler  │
        └──────────────────┘             └──────────────────┘
                  ⚫                               ⚫
                  │                               │
                  │                               │
        ┌─────────┴────────┐             ┌────────┴─────────┐ 
        │  EntityAService  │       ┌────⚫│  EntityBService  │
        └──────────────────┘       │     └──────────────────┘
                  ⚫                │              ⚫
                  │                │              │
                  │                │              │
        ┌─────────┴─────────┐      │      ┌───────┴───────────┐ 
        │ EntityARepository │──────┘      │ EntityBRepository │
        └───────────────────┘             └───────────────────┘
```

Same layer dependency example (BAD PRACTICE):

```
                              ┌──────┐
                  ┌──────────⚫│ main │⚫───────────┐
                  │           └──────┘            │
                  │                               │
                  │                               │
        ┌─────────┴────────┐             ┌────────┴─────────┐                │
        │  EntityAHandler  │             │  EntityBHandler  │
        └──────────────────┘             └──────────────────┘
                  ⚫                               ⚫
                  │                               │
                  │                               │
        ┌─────────┴────────┐             ┌────────┴─────────┐ 
        │  EntityAService  │────────────⚫│  EntityBService  │
        └──────────────────┘             └──────────────────┘
                  ⚫                               ⚫
                  │                               │
                  │                               │
        ┌─────────┴─────────┐             ┌───────┴───────────┐ 
        │ EntityARepository │⚫────────────│ EntityBRepository │
        └───────────────────┘             └───────────────────┘
```

#### Acyclic logical / physical coherence

Create components/libs by entities. Except for generic functional/tool libraries (such ad bdl).

Treat each physical aggregate, at least architecturally, as atomic.

It facilitates human cognition and reasoning.
Facilitates reuse.
Facilitates scalability and load balancing.

Try to make dependencies between libraries one-sense. e.g. Make reservation depend on vehicle but do not make reservation depend on vehicle and vehicle depend on reservation. Same for database tables. Prevent from cross dependency between libraries.