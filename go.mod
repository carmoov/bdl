module gitlab.com/carmoov/bdl

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/mitchellh/mapstructure v1.3.1
	github.com/stretchr/testify v1.6.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/tools v0.0.0-20200529172331-a64b76657301 // indirect
)
