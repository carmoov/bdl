//
// Copyright (c) carmoov
//
// Mutex Pool allows threads associated with
// a given key to be atomic between them but
// not with threads associated with a different key.
//
package sync

import (
	"sync"
)

//
// Mutex Pool allow threads associated with
// a given key to be atomic between them but
// not with threads associated with a different key.
//
type MutexPool struct {
	mtx		sync.Mutex
	pool	map[string]*sync.Mutex
}

//
// Returns a mutex pointer associated with a key,
// if mutex with that key does not exist, it creates
// and returns it.
//
func (mutexPool *MutexPool) Get(key string) *sync.Mutex {
	mutexPool.mtx.Lock()
	mtx := mutexPool.pool[key]
	if mtx != nil { // mutex exists
		mutexPool.mtx.Unlock()
		return mtx
	} else { // mutex with given key does not exist, create and store it.
		mtx := &sync.Mutex{}
		if mutexPool.pool == nil {
			mutexPool.pool = make(map[string]*sync.Mutex)
		}
		mutexPool.pool[key] = mtx
		mutexPool.mtx.Unlock()
		return mtx
	}
}