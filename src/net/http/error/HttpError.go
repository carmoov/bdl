//
// Copyright (c) <2020> carmoov
//
// Collection of error codes for http
//
//
package error

const REQUEST_FAILURE string = "request_failure"                 // Generic server request failure
const SERVER_ERROR string = "server_error"                       // The server encountered an unexpected condition that prevented it from fulfilling the request.
const TEMPORARILY_UNAVAILABLE string = "temporarily_unavailable" // The server is currently unable to handle the request due to a temporary overloading or maintenance of the server.

const INVALID_REQUEST string = "invalid_request"                         // The request is missing a required parameter, includes an unsupported parameter value (other than grant type), repeats a parameter, includes multiple credentials, utilizes more than one mechanism for authenticating the client, or is otherwise malformed.
const UNSUPPORTED_RESPONSE_FORMAT string = "unsupported_response_format" // The server returns a response in an unsuported format.

//
// OAuth2 errors
//
const ACCESS_DENIED string = "access_denied"                         // The resource owner or authorization server denied the request.
const INVALID_CLIENT string = "invalid_client"                       // Client authentication failed (e.g., unknown client, no client authentication included, or unsupported authentication method).
const INVALID_GRANT string = "invalid_grant"                         // The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client.
const INVALID_SCOPE string = "invalid_scope"                         // The requested scope is invalid, unknown, malformed, or exceeds the scope granted by the resource owner.
const UNAUTHORIZED_CLIENT string = "unauthorized_client"             // The authenticated client is not authorized to use this authorization grant type.
const UNSUPPORTED_GRANT_TYPE string = "unsupported_grant_type"       // The authorization grant type is not supported by the authorization server.
const UNSUPPORTED_RESPONSE_TYPE string = "unsupported_response_type" // The authorization server does not support obtaining an access token using this method.
