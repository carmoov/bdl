//
// Copyright (c) <2020> carmoov
//
// Functions for parsing HTTP Requests
//
///
package parser

import (
	"errors"
	"strings"
	"net/http"
	http_request_constant "gitlab.com/carmoov/bdl/src/net/http/request/constant"
)

//
// Retrieve Authorization token from request header with given scheme.
// @param HTTPRequest	HTTP Request.
// @param scheme 		Basic or Bearer
///
func AuthorizationToken(HTTPRequest *http.Request, scheme string) (string, error) {

	// get Authorization value from HTTP Request header
	authorizationMap := HTTPRequest.Header[http_request_constant.HEADER_AUTHORIZATION]

	if len(authorizationMap) == 0 {
		return "", errors.New("authorization_header_not_found")
	}

	authorization := authorizationMap[0]

	if authorization == "" {
		return "", errors.New("authorization_header_is_empty")
	}

	// check scheme validity, must be Basic or Bearer
	authorization = strings.TrimSpace(authorization)

	if !strings.HasPrefix(authorization, scheme) {
		return "", errors.New("unknown_authorization_scheme")
	}

	// extract token
	accessToken := strings.ReplaceAll(authorization, scheme, "")
	accessToken = strings.TrimSpace(accessToken)

	if accessToken == "" {
		return "", errors.New("token_not_found")
	}

	return accessToken, nil
}


//
// Retrieve Basic Authorization token from request header.
// @param HTTPRequest	HTTP Request.
///
func BasicAuthorizationToken(HTTPRequest *http.Request) (string, error) {
	return AuthorizationToken(HTTPRequest, http_request_constant.AUTHORIZATION_SCHEME_BASIC)
}


//
// Retrieve Bearer Authorization token from request header.
// @param HTTPRequest	HTTP Request.
///
func BearerAuthorizationToken(HTTPRequest *http.Request) (string, error) {
	return AuthorizationToken(HTTPRequest, http_request_constant.AUTHORIZATION_SCHEME_BEARER)
}