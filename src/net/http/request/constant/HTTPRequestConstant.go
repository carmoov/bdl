//
// Copyright (c) <2020> carmoov
//
// Constants for HTTP Requests
//
///
package constant

const (
	HEADER_AUTHORIZATION = "Authorization"
	AUTHORIZATION_SCHEME_BASIC = "Basic"
	AUTHORIZATION_SCHEME_BEARER = "Bearer"
)