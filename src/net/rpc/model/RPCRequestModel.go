//
// Copyright (c) <2020> carmoov
//
// Request model for rpc.#
//
// Example:
// 	POST	/rpc	HTTP/1.1
//	Content-Type:	application/json
//	Version:	DRAFT#1.0
//	Host:	api.carmoov.io
//	Content-Length:	...
//	{
//		"jsonrpc":	"2.0",
//		"method":	"user.create",
//		"params":	{
//			"firstname":	"john",
//			"lastname":	"doe",
//			"password":	"secret",
//			"email":	"john.doe@email.com",
//			"phonenumber":	"+33000000000",
//			"birthday":	"01/01/1970",
//			"gender":	"M",
//			"locale":	"FR",
//			"licensePlace":	"joinville-le-pont",
//			"address":	{
//				"country":	"France",
// 				"city":	"joinville-le-pont",
//				"street":	"1,	quai	gabriel	peri",
//				"zipCode":	"94340"
//			}
//		},
//		"id":	null
//	}
//
///
package model

type RPCRequest struct {
	JSONRPC *string	`json:"jsonrpc,omitempty"`
	Method 	*string	`json:"method,omitempty"`
	Params 	interface{} `json:"params,omitempty"`
	ID 		*string	`json:"id,omitempty"`
}
