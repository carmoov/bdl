//
// Copyright (c) <2020> carmoov
//
// Responses model for RPC 2.0 standard.
//
// Example:
// 	{
//		"jsonrpc":	"2.0",
//		"result":	"success",
//		"id":	"b86aab10-30ed-457d-a4c1-583742336264"
//	}
//
// Example:
// 	{
//		"jsonrpc":	"2.0",
//		"error":	{
//		 	"code" : 1003,
//			"message" : "Data not found"
//		},
//		"id":	null
//	}
//
///

package model

import (
	rpc_constant "gitlab.com/carmoov/bdl/src/net/rpc/constant"
	ptr_tools "gitlab.com/carmoov/bdl/src/tools/ptr"
)


type RPCResponse struct {
	JSONRPC *string	`json:"jsonrpc,omitempty"`
	Result 	interface{} `json:"result,omitempty"`
	Error 	*RPCError	`json:"error,omitempty"`
	ID 		*string	`json:"id,omitempty"`
}

func NewRPCResponse() *RPCResponse {
	return &RPCResponse{JSONRPC:ptr_tools.String(rpc_constant.JSON_RPC_VERSION_2_0)}
}

func NewRPCErrorResponse(code int, message string, meaning string) *RPCResponse {
	return &RPCResponse{
		JSONRPC:ptr_tools.String(rpc_constant.JSON_RPC_VERSION_2_0),
		Error: NewRPCError(code, message, meaning),
	}
}