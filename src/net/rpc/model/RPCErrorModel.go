//
// Copyright (c) <2020> carmoov
//
// Error model for rpc Response.
//
///
package model

import (
	rpc_constant "gitlab.com/carmoov/bdl/src/net/rpc/constant"
	ptr_tools "gitlab.com/carmoov/bdl/src/tools/ptr"
)

type RPCError struct {
	Code		*int `json:"code,omitempty"`
	Message 	*string	`json:"message,omitempty"`
	Data		interface{} `json:"data,omitempty"`
}

func NewRPCError(code int, message string, meaning string) *RPCError {
	if message == "" {
		return &RPCError{
			Code: ptr_tools.Int(rpc_constant.ERROR_UNKNOWN_SERVER_ERROR_CODE),
			Message: ptr_tools.String(rpc_constant.ERROR_UNKNOWN_SERVER_ERROR_MESSAGE),
			Data: &RPCDataMeaning{
				Meaning: ptr_tools.String(rpc_constant.ERROR_UNKNOWN_SERVER_ERROR_MEANING),
			},
		}
	}

	if meaning == "" {
		return &RPCError{
			Code: ptr_tools.Int(code),
			Message: ptr_tools.String(message),
		}
	}

	return &RPCError{
		Code: ptr_tools.Int(code),
		Message: ptr_tools.String(message),
		Data: &RPCDataMeaning{
			Meaning: ptr_tools.String(meaning),
		},
	}
}