//
// Copyright (c) <2020> carmoov
//
// Data model for Error rpc Response.
// Notice there is no standard for data in RPC error.
// Data: A Primitive or Structured value that contains additional
// information about the error.
// This may be omitted.
// The value of this member is defined by the Server (e.g. detailed
// error information, nested errors etc.).
//
///
package model

type RPCDataMeaning struct {
	Meaning *string `json:"meaning,omitempty"`
}