//
// Copyright (c) <2020> carmoov
///
package constant

const (
	JSON_RPC_VERSION_2_0 = "2.0"
	
	ERROR_PARSE_ERROR_CODE = -32700
	ERROR_PARSE_ERROR_MESSAGE = "Parse error"
	ERROR_PARSE_ERROR_MEANING = "Invalid JSON was received by the server."

	ERROR_INVALID_REQUEST_CODE = -32600
	ERROR_INVALID_REQUEST_MESSAGE = "Invalid Request"
	ERROR_INVALID_REQUEST_MEANING = "The JSON sent is not a valid Request object."

	ERROR_METHOD_NOT_FOUND_CODE = -32601
	ERROR_METHOD_NOT_FOUND_MESSAGE = "Method not found"
	ERROR_METHOD_NOT_FOUND_MEANING = "The method does not exist / is not available."

	ERROR_INVALID_PARAMS_CODE = -32602
	ERROR_INVALID_PARAMS_MESSAGE = "Invalid params"
	ERROR_INVALID_PARAMS_MEANING = "Invalid method parameter(s)."

	ERROR_INTERNAL_ERROR_CODE = -32603
	ERROR_INTERNAL_ERROR_MESSAGE = "Internal error"
	ERROR_INTERNAL_ERROR_MEANING = "Internal JSON-RPC error."

	//
	// -32000 to -32099 	Server error 	Reserved for implementation-defined server-errors.
	///
	ERROR_UNKNOWN_SERVER_ERROR_CODE = -32000
	ERROR_UNKNOWN_SERVER_ERROR_MESSAGE = "Unknown server error"
	ERROR_UNKNOWN_SERVER_ERROR_MEANING = "Reserved for implementation-defined server-errors."
)