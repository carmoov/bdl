//
// Copyright (c) carmoov
//
// Tool for generating bcrypt keys from passwords and compare keys with password.
///
package crypto

import (
	"golang.org/x/crypto/bcrypt"
)

const (
	BCRYPT_SALT_ROUNDS = 10
)

//
// Returns a bcrypt key.
// @param	password 	Password to use to generate key.
// @return				Bcrypt Key or error.
///
func Bcrypt(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	return string(bytes), err
}

//
// Returns true if key is validated with password
// @param password	Password to check.
// @param hash		Bcrypt key.
// @return			True if key is validated, otherwise false.
///
func CompareBcrypt(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
