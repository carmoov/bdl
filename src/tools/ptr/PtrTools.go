//
// Copyright (c) carmoov
//
// Tools for pointers.
//
///
package ptr

//
// Returns the pointer of a string.
// @param	String
// @return	Pointer to given string.
///
func String(str string) *string {
	return &str
}

//
// Returns the pointer of a int.
// @param	int
// @return	Pointer to given int.
///
func Int(integer int) *int {
	return &integer
}

//
// Returns the pointer of a int16.
// @param	int16
// @return	Pointer to given int16.
///
func Int16(integer int16) *int16 {
	return &integer
}

//
// Returns the pointer of a int64.
// @param	int64
// @return	Pointer to given int64.
///
func Int64(integer int64) *int64 {
	return &integer
}

//
// Returns the pointer of a bool.
// @param	bool
// @return	Pointer to given bool.
///
func Bool(boolean bool) *bool {
	return &boolean
}

//
// Returns the pointer of a uint.
// @param	uint
// @return	Pointer to given uint.
///
func Uint(uinteger uint) *uint {
	return &uinteger
}

//
// Returns the pointer of a uint16.
// @param	uint16
// @return	Pointer to given uint16.
///
func Uint16(uinteger uint16) *uint16 {
	return &uinteger
}

//
// Returns the pointer of a uint32.
// @param	uint32
// @return	Pointer to given uint32.
///
func Uint32(uinteger uint32) *uint32 {
	return &uinteger
}

//
// Returns the pointer of a uint64.
// @param	uint64
// @return	Pointer to given uint64.
///
func Uint64(uinteger uint64) *uint64 {
	return &uinteger
}

//
// Returns the pointer of a float32.
// @param	float32
// @return	Pointer to given float32.
///
 func Float32(f float32) *float32 {
	return &f
}

//
// Returns the pointer of a float64.
// @param	float64
// @return	Pointer to given float64.
///
func Float64(f float64) *float64 {
return &f
}
