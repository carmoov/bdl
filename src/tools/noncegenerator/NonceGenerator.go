//
// Copyright (c) carmoov
//
// Tool for generating alphanumeric strings.
///
package noncegenerator

import (
	"math/rand"
	"time"
)

const (
	CHARSET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
)

//
// Returns an alphanumeric hash of a given length.
// @param  length 	Length of the hash to generate.
// @return			Alphanumeric hash of a given length.
// 					Example: hash(32) => "r8f12t3EajX1UwFDICkFkUMS4hw48oEa"
///
func Hash(length int) string {
	var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = CHARSET[seededRand.Intn(62)]
	}
	return string(b)
}
