//
// Copyright (c) carmoov
//
// Tools for numbers.
//
///
package number

import (
	"strings"
	"math/big"
)

//
// Convert integer64 from base 10 to base 32.
// @param 	number	Integer to convert to base 32.
// @return			Converted number.
///
func Int64ToBase32(number int64) string {
	return strings.ToUpper(big.NewInt(number).Text(32))
}