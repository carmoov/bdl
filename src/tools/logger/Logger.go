//
// Copyright (c) carmoov
//
// Wrapping logger to type logs with INFO, WARNING or ERROR
// prefix.
//
///
package logger

import (
	"log"
)

//
// Logs given message with [INFO] prefix.
// @param	message		Message
///
func Info(message ...interface{}) {
	log.Println("[INFO]", message)
}

//
// Logs given message with [WARNING] prefix.
// @param	message		Message
///
func Warning(message ...interface{}) {
	log.Println("[WARNING]", message)
}

//
// Logs given message with [ERROR] prefix.
// @param	message		Message
///
func Error(message ...interface{}) {
	log.Println("[ERROR]", message)
}
