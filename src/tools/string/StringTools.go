//
// Copyright (c) carmoov
//
// Tools for working with strings, replacing content in strings.
//
///
package string

import "strings"

//
// Replace a map of key tags in a string by a value.
// Example:
// 			content:
// 					hello {{username}} !!! {{date}}
// 			Replace map:
// 					username  => "John Doe"
// 					date      => "Tuesday, May 17, 2016"
// 		  Open: {{ , Close: }}
// 			Result:
// 					hello John Doe !!! Tuesday, May 17, 2016
//
// @param  content   Content containing the tags to replace.
// @param  values    Deque with the key-values pairs containing the
//                   values to replace in the file.
// @param  open      Before tag mark.
// @param  close     After tag mark.
///
func OCMapReplace(content *string, values *map[string]string, open string, close string) {
	for key, value := range *values {
		*(content) = strings.ReplaceAll(*(content), open+key+close, value)
	}
}

//
// Replace a map of key tags in a string by a value using before and after
// tag marks: open "{{" and default close "}}"
// @param content ontent containing the tags to replace.
// @param values  Map with the key-values pairs containing the
//                values to replace in the file.
///
func MapReplace(content *string, values *map[string]string) {
	OCMapReplace(content, values, "{{", "}}")
}
