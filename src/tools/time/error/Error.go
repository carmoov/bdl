//
// Copyright (c) <2020> carmoov
//
// Collection of error codes for time
//
///
package error

const TIME_CONVERSION_FAILURE string = "time_conversion_failure"
const MALFORMED_PARAMETERS string = "malformed_parameters"
