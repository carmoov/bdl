//
// Copyright (c) carmoov
//
// Time tools for time retreive and time format conversion
//
///
package time

import (
	"errors"
	"strconv"
	"time"

	time_error "gitlab.com/carmoov/bdl/src/tools/time/error"
)

//
// Returns the actual time in RFC3339 string format.
// @return			String containing the date and time in RFC3339 format.
//					Example: "2020-02-17T10:10:00.273Z".
///
func NowRFC3339() string {
	return time.Now().UTC().Format("2006-01-02T15:04:05.000Z")
}

//
// Convert RFC3339 string date into Timestamp in milliseconds.
// @param	str		String containing the date and time in RFC3339 format.
//					Example: "2020-02-17T10:10:00.273Z".
//
// @return			Timestamp in milliseconds or error code.
//					Example: for given input "2020-02-17T10:10:00.273Z"
//							 will return: 1581934200273.
///
func RFC3339StringToTimestamp(str string) (int64, error) {
	t, err := time.Parse(time.RFC3339, str)

	if err != nil {
		return 0, errors.New(time_error.TIME_CONVERSION_FAILURE)
	}

	return int64(t.UnixNano() / 1000000), nil
}

//
// Convert RFC3339 string date into Timestamp in milliseconds.
// @param	str		String containing the date and time in RFC3339 format.
//					Example: "2020-02-17T10:10:00.273Z".
//
// @return			Timestamp in milliseconds or error code.
//					Example: for given input "2020-02-17T10:10:00.273Z"
//							 will return: "20200217101000".
///
func RFC3339StringToYYYYMMDDhhmmss(str string) (string, error) {

	if len(str) != 24 {
		return "", errors.New(time_error.MALFORMED_PARAMETERS)
	}

	YYYY := str[:4]

	if _, err := strconv.Atoi(YYYY); err != nil {
		return "", errors.New(time_error.MALFORMED_PARAMETERS)
	}

	MM := str[5:7]

	if _, err := strconv.Atoi(MM); err != nil {
		return "", errors.New(time_error.MALFORMED_PARAMETERS)
	}

	DD := str[8:10]

	if _, err := strconv.Atoi(DD); err != nil {
		return "", errors.New(time_error.MALFORMED_PARAMETERS)
	}

	hh := str[11:13]

	if _, err := strconv.Atoi(hh); err != nil {
		return "", errors.New(time_error.MALFORMED_PARAMETERS)
	}

	mm := str[14:16]

	if _, err := strconv.Atoi(mm); err != nil {
		return "", errors.New(time_error.MALFORMED_PARAMETERS)
	}

	ss := str[17:19]

	if _, err := strconv.Atoi(ss); err != nil {
		return "", errors.New(time_error.MALFORMED_PARAMETERS)
	}

	return YYYY + MM + DD + hh + mm + ss, nil
}

//
// Returns the actual time in Timestamp in milliseconds
// @return			Timestamp in milliseconds.
///
func NowTimestamp() int64 {
	return int64(time.Now().UnixNano() / 1000000)
}

//
// Convert Timestamp in milliseconds into RFC3339 string.
// @param	timestamp	Timestamp in milliseconds.
//						Example: 1581934200273
//
// @return				String containing the date and time in RFC3339 format.
//						Example: for given input 1581934200273
//								 will return "2020-02-17T10:10:00.273Z".
///
func TimestampToRFC3339String(timestamp int64) string {
	return time.Unix(0, timestamp*int64(time.Millisecond)).UTC().Format("2006-01-02T15:04:05.000Z")
}

//
// Convert Timestamp in milliseconds into YYYYMMDDhhmmss string.
// @param	timestamp	Timestamp in milliseconds.
//						Example: 1581934200273
//
// @return				String containing the date and time in YYYYMMDDhhmmss format.
//						Example: for given input 1581934200273
//								 will return "20200217101000".
///
func TimestampToYYYYMMDDhhmmss(timestamp int64) string {
	return time.Unix(0, timestamp*int64(time.Millisecond)).UTC().Format("20060102150405")
}

//
// Returns the actual time in DD/MM/YYYY hh:mm format.
// @return			Time in DD/MM/YYYY hh:mm format.
///
func NowDDMMYYhhmm() string {
	return time.Unix(0, NowTimestamp()*int64(time.Millisecond)).UTC().Format("02/01/2006 15:04")
}
