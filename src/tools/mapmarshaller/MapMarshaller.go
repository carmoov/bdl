//
// Copyright (c) carmoov
//
// Tools for unmarshal maps.
// Use structs.Map(obj) to marshal
//
//
package mapmarshaller

import (
	"github.com/mitchellh/mapstructure"
)

//
// Unarshal a given map into an object based on anotations.
// @param 	m		Map to unmarshal.
// @param	obj		Object containing map data.
// @return			Error if any or nil.
///
func Unmarshal(m interface{}, obj interface{}) error {
	return mapstructure.Decode(m, obj)
}
