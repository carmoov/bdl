//
// Copyright (c) carmoov
//
// Tools for working with arrays
//
///
package array

//
// Returns the index of a string in an array of strings.
// Example:
//			arr := [3]string{"How", "are", "you"}
//			(IndexOfString("you",arr) == 2) ==> true
//			(IndexOfString("world",arr) == -1) ==> true
// @param	str		String you want to find its index in an array.
// @param	arr		Array of strings.
// @return			Integer equals to the index of the given string in
//					given array. Returns -1 if given string value is
//					not found in array.
///
 func IndexOfString(str string, arr []string) int {
	for k, v := range arr {
		if str == v {
			return k
		}
	}
	return -1
}
