//
// Copyright (c) carmoov
//
// Tools for generating uuids.
// Use google uuid fork as go doc recomend "github.com/google/uuid"
//
///
package uuid

import (
	"strconv"
	uuid "github.com/google/uuid"
	time_tools "gitlab.com/carmoov/bdl/src/tools/time"
)

//
// Returns a RFC4122 compliant UUID
// @return	RFC4122 compliant UUID
//			Example: be7b8a1d-6a91-01e0-955b-04f8c912b07b
///
func UUID() string {
	return uuid.New().String()
}

//
// Returns a NOT RFC4122 compliant UUID prefixed
// with current timestamp milliseconds.
// @return	NOT RFC4122 compliant UUID prefixed
// 			with current timestamp milliseconds.
//			Example: 1583836169144_be7b8a1d-6a91-01e0-955b-04f8c912b07b
// @return	error if generation failed or nil.
///
func DatetimeUUID() string {
	return strconv.FormatInt(time_tools.NowTimestamp(), 10) + "_" + UUID()
}
