//
// Copyright (c) <2020> carmoov
//
// Tests for time tools.
//
///
package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	tools_number "gitlab.com/carmoov/bdl/src/tools/number"
)


//
// Test conversion of RFC3339 string date into YYYYMMDDhhmmss in milliseconds.
///
func TestIntToBase32(t *testing.T) {

	const number int64 = 100000

	base32Str := tools_number.Int64ToBase32(number)

	assert.EqualValuesf(t, base32Str, "31L0", "Returned value should be equal to `31L0`")
}