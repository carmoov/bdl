//
// Copyright (c) <2020> carmoov
//
// Tests for string tools.
//
///
package test

import (
	"testing"
	"github.com/stretchr/testify/assert"
	tools_string "gitlab.com/carmoov/bdl/src/tools/string"
	tools_time "gitlab.com/carmoov/bdl/src/tools/time"
	tools_uuid "gitlab.com/carmoov/bdl/src/tools/uuid"
)

var requestStr = `<impl:doWebPaymentRequest xmlns:impl="http://impl.ws.payline.experian.com" xmlns:s11="http://schemas.xmlsoap.org/soap/envelope/">
<impl:version>24</impl:version>
<impl:payment>
  <tns1:amount xmlns:tns1="http://obj.ws.payline.experian.com">{{amount}}</tns1:amount>
  <tns1:currency xmlns:tns1="http://obj.ws.payline.experian.com">{{currency}}</tns1:currency>
  <tns1:action xmlns:tns1="http://obj.ws.payline.experian.com">{{payment.action}}</tns1:action>
  <tns1:mode xmlns:tns1="http://obj.ws.payline.experian.com">{{payment.mode}}</tns1:mode>
  <tns1:contractNumber xmlns:tns1="http://obj.ws.payline.experian.com">{{contractNumber}}</tns1:contractNumber>
  <tns1:method xmlns:tns1="http://obj.ws.payline.experian.com">{{payment.method}}</tns1:method>
  <tns1:softDescriptor xmlns:tns1="http://obj.ws.payline.experian.com">{{payment.softDescriptor}}</tns1:softDescriptor>
  <tns1:cardBrand xmlns:tns1="http://obj.ws.payline.experian.com">{{payment.cardBrand}}</tns1:cardBrand>
</impl:payment>
<impl:returnURL>{{returnURL}}</impl:returnURL>
<impl:cancelURL>{{cancelURL}}</impl:cancelURL>
<impl:order>
  <tns1:ref xmlns:tns1="http://obj.ws.payline.experian.com">{{order.ref}}</tns1:ref>
  <tns1:date xmlns:tns1="http://obj.ws.payline.experian.com">{{order.date}}</tns1:date>
  <tns1:country xmlns:tns1="http://obj.ws.payline.experian.com">{{order.country}}</tns1:country>
  <tns1:amount xmlns:tns1="http://obj.ws.payline.experian.com">{{amount}}</tns1:amount>
  <tns1:currency xmlns:tns1="http://obj.ws.payline.experian.com">{{currency}}</tns1:currency>
</impl:order>
<impl:notificationURL/>
<impl:selectedContractList>
  <tns1:selectedContract xmlns:tns1="http://obj.ws.payline.experian.com">{{contractNumber}}</tns1:selectedContract>
</impl:selectedContractList>
<impl:buyer>
  <tns1:title xmlns:tns1="http://obj.ws.payline.experian.com">{{title}}</tns1:title>
  <tns1:lastName xmlns:tns1="http://obj.ws.payline.experian.com">{{lastName}}</tns1:lastName>
  <tns1:firstName xmlns:tns1="http://obj.ws.payline.experian.com">{{firstName}}</tns1:firstName>
  <tns1:email xmlns:tns1="http://obj.ws.payline.experian.com">{{email}}</tns1:email>
  <tns1:shippingAdress xmlns:tns1="http://obj.ws.payline.experian.com">
	<tns1:title>{{title}}</tns1:title>
	<tns1:firstName>{{firstName}}</tns1:firstName>
	<tns1:lastName>{{lastName}}</tns1:lastName>
	<tns1:street1>{{street1}}</tns1:street1>
	<tns1:cityName>{{cityName}}</tns1:cityName>
	<tns1:zipCode>{{zipCode}}</tns1:zipCode>
	<tns1:country>{{country}}</tns1:country>
	<tns1:email>{{email}}</tns1:email>
  </tns1:shippingAdress>
	<tns1:billingAddress xmlns:tns1="http://obj.ws.payline.experian.com">
	<tns1:title>{{title}}</tns1:title>
	<tns1:firstName>{{firstName}}</tns1:firstName>
	<tns1:lastName>{{lastName}}</tns1:lastName>
	<tns1:street1>{{street1}}</tns1:street1>
	<tns1:cityName>{{cityName}}</tns1:cityName>
	<tns1:zipCode>{{zipCode}}</tns1:zipCode>
	<tns1:country>{{country}}</tns1:country>
	<tns1:email>{{email}}</tns1:email>
  </tns1:billingAddress>
  <tns1:customerId xmlns:tns1="http://obj.ws.payline.experian.com">{{customerID}}</tns1:customerId>
  <tns1:legalStatus xmlns:tns1="http://obj.ws.payline.experian.com">{{legalStatus}}</tns1:legalStatus>
  <tns1:birthDate xmlns:tns1="http://obj.ws.payline.experian.com">{{birthDate}}</tns1:birthDate>
  <!--This element may be left empty if xsi:nil='true' is set.-->
  <tns1:merchantAuthentication xmlns:tns1="http://obj.ws.payline.experian.com">
	<!--This element may be left empty if xsi:nil='true' is set.-->
	<tns1:method/>
	<!--This element may be left empty if xsi:nil='true' is set.-->
	<tns1:date/>
  </tns1:merchantAuthentication>
</impl:buyer>
<impl:securityMode>SSL</impl:securityMode>
<impl:threeDSInfo>
  <tns1:challengeInd xmlns:tns1="http://obj.ws.payline.experian.com">04</tns1:challengeInd>
  <tns1:threeDSReqPriorAuthMethod xmlns:tns1="http://obj.ws.payline.experian.com">02</tns1:threeDSReqPriorAuthMethod>
  <tns1:threeDSReqPriorAuthTimestamp xmlns:tns1="http://obj.ws.payline.experian.com">13/03/2020</tns1:threeDSReqPriorAuthTimestamp>
  <tns1:browser xmlns:tns1="http://obj.ws.payline.experian.com">
	<tns1:acceptHeader>text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,//*;q=0.8</tns1:acceptHeader>
	<tns1:javaEnabled>N</tns1:javaEnabled>
	<tns1:javascriptEnabled>Y</tns1:javascriptEnabled>
	<tns1:language>EN</tns1:language>
	<tns1:colorDepth>16</tns1:colorDepth>
	<tns1:screenHeight>600</tns1:screenHeight>
	<tns1:screenWidth>300</tns1:screenWidth>
	<tns1:userAgent>Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19</tns1:userAgent>
  </tns1:browser>
  <tns1:threeDSMethodNotificationURL xmlns:tns1="http://obj.ws.payline.experian.com">https://documents.carmoov.io/</tns1:threeDSMethodNotificationURL>
  <tns1:threeDSMethodResult xmlns:tns1="http://obj.ws.payline.experian.com">Y</tns1:threeDSMethodResult>
</impl:threeDSInfo>
<impl:languageCode>fr</impl:languageCode>
</impl:doWebPaymentRequest>`

//
// Test conversion of RFC3339 string date into Timestamp in milliseconds.
///
func TestMapReplace(t *testing.T) {

	uuid := tools_uuid.UUID()

	var values map[string]string = map[string]string{
		"amount":                 "44",
		"currency":               "978",
		"payment.action":         "101",
		"payment.mode":           "CPT",
		"contractNumber":         "0384528",
		"payment.method":         "CB",
		"payment.softDescriptor": "presto.rentacar.fr",
		"payment.cardBrand":      "CB",
		"returnURL":              "http://localhost:4200/threeDRedirection",
		"cancelURL":              "http://localhost:4200/threeDRedirection",
		"order.ref":              "TEST_GO_05",
		"order.date":             tools_time.NowRFC3339(),
		"order.country":          "FR",
		"title":                  "M",
		"lastName":               "Doe",
		"firstName":              "John",
		"email":                  "john.doe@example.com",
		"street1":                "76 rue de la pie",
		"cityName":               "Paris",
		"zipCode":                "75001",
		"country":                "FR",
		"customerID":             uuid,
		"legalStatus":            "person",
		"birthDate":              tools_time.NowRFC3339(),
	}

	tools_string.MapReplace(&requestStr, &values)

	assert.EqualValuesf(t, len(requestStr), 4905, "String with replaced values should be 4905 characters length.")

}
