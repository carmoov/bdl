//
// Copyright (c) carmoov
//
// Tests for map tools .
//
///
package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	map_marshaller "gitlab.com/carmoov/bdl/src/tools/mapmarshaller"
)

type User struct {
	Email      *string   `json:"email"`
	FirstName  *string   `json:"firstname"`
	LastName   *string   `json:"lastname"`
}

func TestUnmarshal(t *testing.T) {

	email := "john.doe@example.com"

	m := make(map[string]string)
	m["email"] = email
	
	var user User

	err := map_marshaller.Unmarshal(&m, &user)

	assert.Nil(t, err)

	if err != nil { return }

	assert.Equal(t, *user.Email, m["email"], "Object email and map email should match.")
}
  