//
// Copyright (c) carmoov
//
// Tests for crypto .
//
///
package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/carmoov/bdl/src/crypto"
)

func TestBcrypt(t *testing.T) {
	password := "password"
	key, err := crypto.Bcrypt(password)
	assert.Nil(t, err)

	if err != nil { return }

	assert.Equal(t, crypto.CompareBcrypt(password, key), true, "password should match key")
}

func TestBcryptError(t *testing.T) {
	password := "password"
	key, err := crypto.Bcrypt(password)
	assert.Nil(t, err)

	if err != nil { return }

	assert.Equal(t, crypto.CompareBcrypt("wrong password", key), false, "password should match key")
}

 