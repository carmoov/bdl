//
// Copyright (c) carmoov
//
// Tests for Mutex pool.
//
//
package test

import (
	"sync"
	"time"
	"testing"
	"github.com/stretchr/testify/assert"
	bdl_sync "gitlab.com/carmoov/bdl/src/sync"
)

var mtxPool bdl_sync.MutexPool
var threadMtx sync.Mutex
var threadExecutedCounter int = 0
var testMutexPoolCounter int = 0
var testMutexPoolCounter2 int = 0

func increaseCounter(key string, counter *int){
	mtx := mtxPool.Get(key)
	mtx.Lock()
	*counter = *counter + 1
	mtx.Unlock()

	threadMtx.Lock()
	threadExecutedCounter = threadExecutedCounter + 1
	threadMtx.Unlock()
}

//
// Test mutex pool with 2 mutex but threads
// increasing a variable per mutex.
// There is always a probability of passing the test and having
// a not working Mutex Pool.
//
func TestMutexPool(t *testing.T) {

	threadExecutedCounter = 0
	testMutexPoolCounter = 0
	testMutexPoolCounter2 = 0
	
	key := "LD0000"
	key2 := "LD0001"

	for i := 1; i < 10000; i++ {
		go increaseCounter(key, &testMutexPoolCounter)
		go increaseCounter(key2, &testMutexPoolCounter2)
	}
	
	for threadExecutedCounter < 19998 {
		time.Sleep(time.Second)
	}
	// there is always a probability of passing the test and having
	// a not working Mutex Pool
	assert.Equal(t, 9999, testMutexPoolCounter, "counter1 should be equal to 9999")
	assert.Equal(t, 9999, testMutexPoolCounter2, "counter2 should be equal to 9999")
}

//
// Test mutex pool with 2 mutex but threads
// increasing same variable.
// There is always a probability of not passing the test.
//
func TestMutexPoolError(t *testing.T) {

	threadExecutedCounter = 0
	testMutexPoolCounter = 0
	testMutexPoolCounter2 = 0
	
	key := "LD0000"
	key2 := "LD0001"

	for i := 1; i < 10000; i++ {
		go increaseCounter(key, &testMutexPoolCounter)
		go increaseCounter(key2, &testMutexPoolCounter)
	}

	for threadExecutedCounter < 19998 {
		time.Sleep(time.Second)
	}
	assert.NotEqual(t, 19998, testMutexPoolCounter, "counter should not be equal to 19998")
}