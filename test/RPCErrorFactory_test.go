//
// Copyright (c) carmoov
//
// Tests for RPC error creation.
//
///
package test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	rpc_model "gitlab.com/carmoov/bdl/src/net/rpc/model"
	rpc_error "gitlab.com/carmoov/bdl/src/net/rpc/constant"
)

func TestAuthorizeWrongScope(t *testing.T) {
	rpcError := rpc_model.NewRPCError(rpc_error.ERROR_PARSE_ERROR_CODE, rpc_error.ERROR_PARSE_ERROR_MESSAGE, rpc_error.ERROR_PARSE_ERROR_MEANING)
	assert.Equal(t, *(rpcError.Code), -32700, "Error code should be equal to `-32700`")
}