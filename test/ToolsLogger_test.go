//
// Copyright (c) <2020> carmoov
//
// Tests for logger tool.
//
///
package test

import (
	"testing"

	"gitlab.com/carmoov/bdl/src/tools/logger"
)

//
// Test logger.Info
///
func TestLoggerInfo(t *testing.T) {
	logger.Info("User with id =>", 9387598437, "has been Deleted from database.")
}

//
// Test logger.Warning
///
func TestLoggerWarning(t *testing.T) {
	logger.Warning("User with id =>", 9387598437, "has been Deleted from database, but it's dependencies have not. TODO.")
}

//
// Test logger.Error
///
func TestLoggerError(t *testing.T) {
	logger.Error("Error deleting User with id =>", 9387598437, "from database.")
}
 