//
// Copyright (c) <2020> carmoov
//
// Tests for time tools.
//
///
package test

import (
	"testing"
	"github.com/stretchr/testify/assert"
	tools_time "gitlab.com/carmoov/bdl/src/tools/time"
)

//
// Test NowTimestamp()
///
 func TestNowTimestamp(t *testing.T) {
	now := tools_time.NowTimestamp()
	assert.EqualValuesf(t, (now>1590874777000), true, "Now should be bigger than 1590874777000.")
	assert.EqualValuesf(t, (now<4115318400000), true, "Now should be smaller than 4115318400000.")
}


//
// Test conversion of RFC3339 string date into Timestamp in milliseconds.
///
func TestRFC3339StringToTimestamp(t *testing.T) {

	const str string = "2020-02-17T10:10:00.273Z"

	timestamp, err := tools_time.RFC3339StringToTimestamp(str)

	assert.Nil(t, err)
	assert.NotNil(t, timestamp)
	assert.EqualValuesf(t, timestamp, 1581934200273, "Returned timestamp should be equal to `1581934200273`")

}

//
// Test conversion of RFC3339 string date into YYYYMMDDhhmmss in milliseconds.
///
func TestRFC3339StringToYYYYMMDDhhmmss(t *testing.T) {

	const str string = "2020-02-17T10:10:00.273Z"

	YYYYMMDDhhmmss, err := tools_time.RFC3339StringToYYYYMMDDhhmmss(str)

	assert.Nil(t, err)
	assert.NotNil(t, YYYYMMDDhhmmss)
	assert.EqualValuesf(t, YYYYMMDDhhmmss, "20200217101000", "Returned YYYYMMDDhhmmss should be equal to `20200217101000`")
}

//
// Test conversion of Timestamp in milliseconds into RFC3339 string.
///
func TestTimestampToRFC3339String(t *testing.T) {

	const timestamp int64 = 1581934200273

	datetimeStr := tools_time.TimestampToRFC3339String(timestamp)

	assert.Equal(t, datetimeStr, "2020-02-17T10:10:00.273Z", "Returned string should be equal to `2020-02-17T10:10:00.273Z`")

}

//
// Test conversion of Timestamp in milliseconds into YYYYMMDDhhmmss string.
///
func TestTimestampToYYYYMMDDhhmmss(t *testing.T) {

	const timestamp int64 = 1581934200273

	YYYYMMDDhhmmss := tools_time.TimestampToYYYYMMDDhhmmss(timestamp)

	assert.NotNil(t, YYYYMMDDhhmmss)
	assert.EqualValuesf(t, YYYYMMDDhhmmss, "20200217101000", "Returned YYYYMMDDhhmmss should be equal to `20200217101000`")

}

//
// Test NowDDMMYYhhmm()
///
 func TestNowDDMMYYhhmm(t *testing.T) {
	DDMMYYhhmm := tools_time.NowDDMMYYhhmm()
	assert.EqualValuesf(t, len(DDMMYYhhmm), 16, "Returned YYYYMMDDhhmmss should be 16 characters length.")
}

