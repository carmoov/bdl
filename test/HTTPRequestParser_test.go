//
// Copyright (c) carmoov
//
// Tests for HTTP requests parser.
//
///
package test

import (
	"testing"
	"net/http"
	"github.com/stretchr/testify/assert"
	"gitlab.com/carmoov/bdl/test/mock"
	http_request_parser "gitlab.com/carmoov/bdl/src/net/http/request/parser"
	http_request_constant "gitlab.com/carmoov/bdl/src/net/http/request/constant"
)

func TestHTTPRequestParserAuthorizationToken(t *testing.T) {
	httpRequest := mock.BearerAuthorizationHeaderHTTPRequest()
	token, err := http_request_parser.AuthorizationToken(httpRequest, http_request_constant.AUTHORIZATION_SCHEME_BEARER)

	assert.Nil(t, err)

	if err != nil {return}

	assert.Equal(t, len(token), 32, "Lenght of token should be equal to `32`")

	httpRequest = mock.BasicAuthorizationHeaderHTTPRequest()
	token, err = http_request_parser.AuthorizationToken(httpRequest, http_request_constant.AUTHORIZATION_SCHEME_BASIC)

	assert.Nil(t, err)

	if err != nil {return}

	assert.Equal(t, len(token), 32, "Lenght of token should be equal to `32`")
}

func TestHTTPRequestParserAuthorizationTokenErrors(t *testing.T) {
	
	// no header
	httpRequest := mock.NoHeaderHTTPRequest()
	token, err := http_request_parser.AuthorizationToken(httpRequest, http_request_constant.AUTHORIZATION_SCHEME_BEARER)
	assert.NotNil(t, err)
	if err == nil {return}
	assert.Equal(t, token, "", "token should be an empty string.")
	assert.Equal(t, err.Error(), "authorization_header_not_found", "Error should be equal to `authorization_header_not_found`")

	// header but no Authorization header
	header := http.Header{}
	header["Other-Content"] = []string{"other content"}
	httpRequest = mock.HTTPRequest(&header)
	token, err = http_request_parser.AuthorizationToken(httpRequest, http_request_constant.AUTHORIZATION_SCHEME_BASIC)
	assert.NotNil(t, err)
	if err == nil {return}
	assert.Equal(t, token, "", "token should be an empty string.")
	assert.Equal(t, err.Error(), "authorization_header_not_found", "Error should be equal to `authorization_header_not_found`")

	// Authorization header exist but no content
	header = http.Header{}
	header["Authorization"] = []string{""}
	httpRequest = mock.HTTPRequest(&header)
	token, err = http_request_parser.AuthorizationToken(httpRequest, http_request_constant.AUTHORIZATION_SCHEME_BASIC)
	assert.NotNil(t, err)
	if err == nil {return}
	assert.Equal(t, token, "", "token should be an empty string.")
	assert.Equal(t, err.Error(), "authorization_header_is_empty", "Error should be equal to `authorization_header_is_empty`")

	// Authorization header exist but no scheme
	header = http.Header{}
	header[http_request_constant.HEADER_AUTHORIZATION] = []string{"wrong content"}
	httpRequest = mock.HTTPRequest(&header)
	token, err = http_request_parser.AuthorizationToken(httpRequest, http_request_constant.AUTHORIZATION_SCHEME_BASIC)
	assert.NotNil(t, err)
	if err == nil {return}
	assert.Equal(t, token, "", "token should be an empty string.")
	assert.Equal(t, err.Error(), "unknown_authorization_scheme", "Error should be equal to `unknown_authorization_scheme`")

	// Authorization header exist with wrong scheme
	httpRequest = mock.BearerAuthorizationHeaderHTTPRequest()
	token, err = http_request_parser.AuthorizationToken(httpRequest, http_request_constant.AUTHORIZATION_SCHEME_BASIC)
	assert.NotNil(t, err)
	if err == nil {return}
	assert.Equal(t, token, "", "token should be an empty string.")
	assert.Equal(t, err.Error(), "unknown_authorization_scheme", "Error should be equal to `unknown_authorization_scheme`")

	// Authorization header exist with good scheme but no token
	header = http.Header{}
	header[http_request_constant.HEADER_AUTHORIZATION] = []string{http_request_constant.AUTHORIZATION_SCHEME_BASIC}
	httpRequest = mock.HTTPRequest(&header)
	token, err = http_request_parser.AuthorizationToken(httpRequest, http_request_constant.AUTHORIZATION_SCHEME_BASIC)
	assert.NotNil(t, err)
	if err == nil {return}
	assert.Equal(t, token, "", "token should be an empty string.")
	assert.Equal(t, err.Error(), "token_not_found", "Error should be equal to `token_not_found`")

}


func TestHTTPRequestParserBearerAuthorizationToken(t *testing.T) {
	httpRequest := mock.BearerAuthorizationHeaderHTTPRequest()
	token, err := http_request_parser.BearerAuthorizationToken(httpRequest)

	assert.Nil(t, err)

	if err != nil {return}

	assert.Equal(t, len(token), 32, "Lenght of token should be equal to `32`")

}

func TestHTTPRequestParserBasicAuthorizationToken(t *testing.T) {
	httpRequest := mock.BasicAuthorizationHeaderHTTPRequest()
	token, err := http_request_parser.BasicAuthorizationToken(httpRequest)

	assert.Nil(t, err)

	if err != nil {return}

	assert.Equal(t, len(token), 32, "Lenght of token should be equal to `32`")

}