//
// Copyright (c) <2020> carmoov
//
// Mock Go HTTP Request for testing purposes
//
///
package mock

import (
	"net/http"
	nonce_tools "gitlab.com/carmoov/bdl/src/tools/noncegenerator"
	http_request_constant "gitlab.com/carmoov/bdl/src/net/http/request/constant"
)

//
// Returns an HTTP request with given header
// @param		Header.
// @return 		HTTP request.
///
func HTTPRequest(header *http.Header) *http.Request {
	return &http.Request{
		Header: *header,
	}
}

//
// Returns an HTTP request without header.
// @return 		HTTP request.
///
func NoHeaderHTTPRequest() *http.Request {
	return &http.Request{}
}

//
// Returns an HTTP request with Basic authorization
// token in its header.
// @return 		HTTP request.
///
func BasicAuthorizationHeaderHTTPRequest() *http.Request {
	header := http.Header{}
	header[http_request_constant.HEADER_AUTHORIZATION] = []string{http_request_constant.AUTHORIZATION_SCHEME_BASIC + " " + nonce_tools.Hash(32)}
	return HTTPRequest(&header)
}

//
// Returns an HTTP request with Bearer authorization
// token in its header.
// @return 		HTTP request.
///
func BearerAuthorizationHeaderHTTPRequest() *http.Request {
	header := http.Header{}
	header[http_request_constant.HEADER_AUTHORIZATION] = []string{http_request_constant.AUTHORIZATION_SCHEME_BEARER + " " + nonce_tools.Hash(32)}
	return HTTPRequest(&header)
}