//
// Copyright (c) carmoov
//
// Tests for array tools.
//
///
package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/carmoov/bdl/src/tools/array"
)

//
// Test when string value exists in array.
///
func TestStringExistInArray(t *testing.T) {
	assert.Equal(t, array.IndexOfString("you", []string{"how", "are", "you"}), 2, "Returned index should be equal to `2`")
}

//
// Test when string value does not exists in array.
///
func TestStringDoesNotExistInArray(t *testing.T) {
	assert.Equal(t, array.IndexOfString("world", []string{"how", "are", "you"}), -1, "Returned index should be equal to `2`")
}
