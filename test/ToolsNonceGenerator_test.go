//
// Copyright (c) carmoov
//
// Tests for nonce generator tools .
//
///
package test

import (
	"testing"
	"github.com/stretchr/testify/assert"
	nonce_generator "gitlab.com/carmoov/bdl/src/tools/noncegenerator"
)


func TestNonceGeneratorHash(t *testing.T) {

	hash := nonce_generator.Hash(32)

	assert.Equal(t, len(hash), 32, "Generated hash should be 32 characters length.")
}
