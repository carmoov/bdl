//
// Copyright (c) <2020> carmoov
//
// Tests for time tools.
//
///
package test

import (
	"testing"
	"github.com/stretchr/testify/assert"
	uuid_tools "gitlab.com/carmoov/bdl/src/tools/uuid"
)

//
// Test UUID creation.
///
func TestUUID(t *testing.T) {

	uuid := uuid_tools.UUID()

	k := 0
	for _, c := range uuid {
		if c == 45 {
			k++
		}
	}

	assert.Equal(t, k, 4, "Returned string should have `be7b8a1d-6a91-01e0-955b-04f8c912b07b` format")

}

//
// Test DatetimeUUID creation.
///
func TestDatetimeUUID(t *testing.T) {

	uuid := uuid_tools.DatetimeUUID()

	k := 0
	l := 0
	for _, c := range uuid {
		if c == 45 {
			k++
		} else if c == 95 {
			l++
		}
	}

	assert.Equal(t, k, 4, "Returned string should have `1583836169144_be7b8a1d-6a91-01e0-955b-04f8c912b07b` format")
	assert.Equal(t, l, 1, "Returned string should have `1583836169144_be7b8a1d-6a91-01e0-955b-04f8c912b07b` format")

}
