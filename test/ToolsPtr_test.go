//
// Copyright (c) <2020> carmoov
//
// Tests for ptr tools.
//
///
package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/carmoov/bdl/src/tools/ptr"
)

//
// Test logger.Info
///
func TestStringPtr(t *testing.T) {
	assert.Equal(t, *ptr.String("hello"), "hello", "Value of the returned//string should be equal to `hello`")
}
