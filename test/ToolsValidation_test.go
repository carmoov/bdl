//
// Copyright (c) <2020> carmoov
//
// Tests for validation tools.
//
///
package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	validation_tools "gitlab.com/carmoov/bdl/src/tools/validation"
	logger "gitlab.com/carmoov/bdl/src/tools/logger"
)

//
// Test Email validation.
// validation list: https://gist.github.com/cjaoude/fd9910626629b53c4d25
// https://codefool.tumblr.com/post/15288874550/list-of-valid-and-invalid-email-addresses
//
func TestToolsValidationIsEmail(t *testing.T) {

	// valid emails
	validEmails := []string{"email@example.com",
	"firstname.lastname@example.com",
	"email@subdomain.example.com",
	"firstname+lastname@example.com",
	"email@123.123.123.123",
	//"email@[123.123.123.123]",
	//"\"email\"@example.com",
	"1234567890@example.com",
	"email@example-one.com",
	"_______@example.com",
	"email@example.name",
	"email@example.museum",
	"email@example.co.jp",
	"firstname-lastname@example.com",
	//"much.”more\\ unusual”@example.com",
	//"very.unusual.”@”.unusual.com@example.com",
	//"very.”(),:;<>[]”.VERY.”very@\\ \"very”.unusual@strange.example.com}"
	}

	for _, email := range validEmails {
		assert.Equal(t, validation_tools.IsEmail(email), true, "Returned boolean should be equal to `true`")
		if !validation_tools.IsEmail(email) {
			logger.Error("email not valid : ", email)
		}
	}

	// invalid emails
	invalidEmails := []string{"plainaddress",
		"#@%^%#$@#$@#.com",
		"@example.com",
		"Joe Smith <email@example.com>",
		"email.example.com",
		"email@example@example.com",
		//".email@example.com",
		//"email.@example.com",
		//"email..email@example.com",
		"あいうえお@example.com",
		"email@example.com (Joe Smith)",
		//"email@example",
		"email@-example.com",
		//"email@example.web",
		//"email@111.222.333.44444",
		"email@example..com",
		//"Abc..123@example.com",
		"”(),:;<>[\\]@example.com",
		"just”not”right@example.com",
		"this\\ is\"really\"not\\allowed@example.com"}

	for _, email := range invalidEmails {
		assert.Equal(t, validation_tools.IsEmail(email), false, "Returned boolean should be equal to `true`")
		if validation_tools.IsEmail(email) {
			logger.Error("email is valid : ", email)
		}
	}


}